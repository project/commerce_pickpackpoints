<?php

/**
 * @file
 * Define Pick-pack pont Pane to checkout process.
 */

/**
 * Pick-pack pont Pane: form callback.
 */
function commerce_pickpackpoints_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  if (!commerce_pickpackpoints_shipping_is_pickpackpoints($order)) {
    // No pick-pack shipping.
    $referer = explode('/', $_SERVER['HTTP_REFERER']);
    if (end($referer) == $form_state['checkout_page']['next_page']) {
      $order = commerce_order_status_update($order, 'checkout_' . $form_state['checkout_page']['prev_page']);
      drupal_goto(commerce_checkout_order_uri($order));
    }
    if (end($referer) == $form_state['checkout_page']['prev_page']) {
      $order = commerce_order_status_update($order, 'checkout_' . $form_state['checkout_page']['next_page']);
      drupal_goto(commerce_checkout_order_uri($order));
    }
  }
  $form_state['checkout_pane'] = $checkout_pane['pane_id'];
  $lists = commerce_pickpackpoints_get_lists();

  $county = $city = $shop = 'none';
  $city_options = _commerce_pickpackpoints_get_city_options($lists, $form_state);
  $shop_options = _commerce_pickpackpoints_get_shop_options($lists, $form_state);
  $ppp_value = isset($order->pickpack_field_pickpack[LANGUAGE_NONE][0]['value']) ? $order->pickpack_field_pickpack[LANGUAGE_NONE][0]['value'] : NULL;
  if (!isset($form_state['input']['checkout_pane_pickpackpoints']['county'])) {
    $county = isset($ppp_value) ? $lists['pp_list_reverse'][$ppp_value]['county'] : 'none';
    $city = isset($ppp_value) ? $lists['pp_list_reverse'][$ppp_value]['city'] : 'none';
    $shop = isset($ppp_value) ? $ppp_value : 'none';
  }
  else {
    $county = isset($form_state['input']['checkout_pane_pickpackpoints']['county']) ? $form_state['input']['checkout_pane_pickpackpoints']['county'] : 'none';
    if ($county != 'none') {
      $city = isset($form_state['input']['checkout_pane_pickpackpoints']['elements']['city']) ? $form_state['input']['checkout_pane_pickpackpoints']['elements']['city'] : 'none';
      if ($city != 'none') {
        $shop = isset($form_state['input']['checkout_pane_pickpackpoints']['elements']['shop']) ? $form_state['input']['checkout_pane_pickpackpoints']['elements']['shop'] : 'none';
      }
    }
  }

  $ajax = array(
    'callback' => 'commerce_pickpackpoints_choice_county_js',
    'wrapper' => 'ppp-datapicker-elements-wrapper',
  );

  $form['county'] = array(
    '#type' => 'select',
    '#title' => t('County'),
    '#default_value' => $county,
    '#options' => $lists['pp_county'],
    '#ajax' => $ajax,
  );

  $form['elements'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="ppp-datapicker-elements-wrapper">',
    '#suffix' => '</div>',
  );

  if (!isset($city_options[$city])) {
    $city = 'none';
  }
  $form['elements']['city'] = array(
    '#type' => 'select',
    '#title' => t('City'),
    '#default_value' => $city,
    '#options' => $city_options,
    '#ajax' => $ajax,
    '#disabled' => isset($form_state['input']['checkout_pane_pickpackpoints']['county']) && $form_state['input']['checkout_pane_pickpackpoints']['county'] != 'none' ? FALSE : TRUE,
  );

  if (!isset($shop_options[$shop])) {
    $shop = 'none';
  }
  $form['elements']['shop'] = array(
    '#type' => 'select',
    '#title' => t('Shop'),
    '#default_value' => $shop,
    '#options' => $shop_options,
    '#required' => TRUE,
    '#ajax' => $ajax,
    '#disabled' => isset($form_state['input']['checkout_pane_pickpackpoints']['elements']['city']) && $form_state['input']['checkout_pane_pickpackpoints']['elements']['city'] != 'none' ? FALSE : TRUE,
  );

  $shop_data = _commerce_pickpackpoints_get_shop_render($lists, $form_state);
  if (empty($shop_data) && !empty($lists['pp_rendered_shop'][$shop])) {
    $shop_data = $lists['pp_rendered_shop'][$shop];
  }
  $form['elements']['shop_info'] = array(
    '#markup' => $shop_data,
  );

  return $form;
}

/**
 * Ajax callback from county select.
 */
function commerce_pickpackpoints_choice_county_js($form, $form_state, $checkout_pane, $order) {
  return $form[$form_state['checkout_pane']]['elements'];
}

/**
 * Helper for generating city options.
 */
function _commerce_pickpackpoints_get_city_options($lists, $form_state) {
  if (isset($form_state['input'][$form_state['checkout_pane']]['county']) && $form_state['input'][$form_state['checkout_pane']]['county'] != 'none') {
    $county = $form_state['values'][$form_state['checkout_pane']]['county'];
    if (!empty($lists['pp_city'][$county])) {
      $list['none'] = t('Choose a city');
      $cities = $lists['pp_city'][$county];
      if (is_array($cities)) {
        $list += $cities;
      }
      return $list;
    }
  }
  return $lists['pp_all_cities'];
}

/**
 * Helper for generating shop options.
 */
function _commerce_pickpackpoints_get_shop_options($lists, $form_state) {
  if (isset($form_state['input'][$form_state['checkout_pane']]['elements']['city']) && $form_state['input'][$form_state['checkout_pane']]['elements']['city'] != 'none') {
    $county = $form_state['values'][$form_state['checkout_pane']]['county'];
    $city = $form_state['values'][$form_state['checkout_pane']]['elements']['city'];
    if (!empty($lists['pp_list'][$county][$city])) {
      $list['none'] = t('Choose a shop');
      $shops = $lists['pp_list'][$county][$city];
      if (is_array($shops)) {
        $list += $shops;
      }
      return $list;
    }
  }
  return $lists['pp_all_shops'];
}

/**
 * Helper for display rendered shop data.
 */
function _commerce_pickpackpoints_get_shop_render($lists, $form_state) {
  if (isset($form_state['values'][$form_state['checkout_pane']]['elements']['shop']) && $form_state['values'][$form_state['checkout_pane']]['elements']['shop'] != 'none') {
    $shop = $form_state['values'][$form_state['checkout_pane']]['elements']['shop'];
    if (!empty($lists['pp_rendered_shop'][$shop])) {
      return $lists['pp_rendered_shop'][$shop];
    }
  }
  return '';
}

/**
 * Pick-pack pont pane: validation callback.
 */
function commerce_pickpackpoints_pane_checkout_form_validate($form, &$form_state, $checkout_pane, $order) {
  $shop_id = $form_state['values'][$checkout_pane['pane_id']]['elements']['shop'];
  if ($shop_id == 'none') {
    form_set_error($checkout_pane['pane_id'] . '][shop', t('Please choose a shop.'));
    return FALSE;
  }
  return TRUE;
}

/**
 * Pick-pack pont pane: checkout form submission callback.
 */
function commerce_pickpackpoints_pane_checkout_form_submit($form, &$form_state, $checkout_pane, $order) {
  if (!empty($form_state['values'][$checkout_pane['pane_id']])) {
    $pane_values = $form_state['values'][$checkout_pane['pane_id']];
    $order->data['pick_pack_shop'] = $pane_values['elements']['shop'];
    if (module_exists('commerce_pickpackpoints_field')) {
      $order->pickpack_field_pickpack[LANGUAGE_NONE][0]['value'] = $pane_values['elements']['shop'];
    }
  }
}

/**
 * Pick-pack pont pane.
 *
 * Presents the information we've collected in the Review checkout pane.
 */
function commerce_pickpackpoints_pane_review($form, $form_state, $checkout_pane, $order) {
  if (!commerce_pickpackpoints_shipping_is_pickpackpoints($order)) {
    return '';
  }
  $data = commerce_pickpackpoints_get_data();
  $content = array(
    'nickname' => array(
      '#type' => 'item',
      '#title' => t('Products delivery to Pick-pack pont') . ':',
      '#markup' => commerce_pickpackpoints_get_rendered_data($order->data['pick_pack_shop'], $data[$order->data['pick_pack_shop']]),
    ),
  );
  return drupal_render($content);
}
